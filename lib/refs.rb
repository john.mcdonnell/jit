# frozen_string_literal: true
require "lockfile"
# Refs represents references
class Refs
  attr_accessor :pathname

  def initialize(pathname)
    @pathname = pathname
  end

  LockDenied = Class.new(StandardError)
  def update_head(oid)
    lockfile = Lockfile.new(head_path)
    raise LockDenied, "Could not acquire lock on file: #{head_path}" unless lockfile.hold_for_update

    lockfile.write(oid)
    lockfile.write("\n")
    lockfile.commit
  end

  def read_head
    return unless File.exist?(head_path)

    File.read(head_path).strip
  end

  private

  def head_path
    @pathname.join("HEAD")
  end
end
