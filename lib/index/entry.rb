# frozen_string_literal: true

class Index
  INDEX_REGULAR_MODE    = 0o100644
  INDEX_EXECUTABLE_MODE = 0o100755
  MAX_PATH_SIZE = 0xfff

  # HEADER_FORMAT Array#pack
  #   N10 means ten 32-bit unsigned big-endian numbers”
  #   H40 means a 40-character hex string, which will pack down to 20 bytes
  #   n means a 16-bit unsigned big-endian number
  #   Z* means a null-terminated string
  ENTRY_FORMAT = "N10H40nZ*"
  ENTRY_BLOCK  = 8

  entry_fields = %i[
    ctime ctime_nsec
    mtime mtime_nsec
    dev ino mode uid gid size
    oid flags path
  ]

  Entry = Struct.new(*entry_fields) do
    def self.create(pathname, oid, stat)
      path  = pathname.to_s
      mode  = stat.executable? ? INDEX_EXECUTABLE_MODE : INDEX_REGULAR_MODE
      flags = [path.bytesize, MAX_PATH_SIZE].min

      Entry.new(
        stat.ctime.to_i, stat.ctime.nsec,
        stat.mtime.to_i, stat.mtime.nsec,
        stat.dev, stat.ino, mode, stat.uid, stat.gid, stat.size,
        oid, flags, path
      )
    end

    def key
      path
    end

    def to_s
      string = to_a.pack(ENTRY_FORMAT)
      string.concat("\0") until (string.bytesize % ENTRY_BLOCK).zero?
      string
    end
  end
end
