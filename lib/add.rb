# frozen_string_literal: true

require "index"
require "database/blob"
require "database"

# Add files to the index/staging area prior to committing
class Add

  def initialize
    root_path = Pathname.new(Dir.getwd)
    git_path  = root_path.join(".git")

    workspace = Workspace.new(root_path)
    database  = Database.new(git_path.join("objects"))
    index     = Index.new(git_path.join("index"))

    ARGV.each do |path|
      path = Pathname.new(File.expand_path(path))

      workspace.list_files(path).each do |pathname|
        data = workspace.read_file(pathname)
        stat = workspace.stat_file(pathname)

        blob = Database::Blob.new(data)
        database.store(blob)
        index.add(pathname, blob.oid, stat)
      end
    end

    index.write_updates
    exit 0
  end
end
