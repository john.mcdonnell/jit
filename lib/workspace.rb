# frozen_string_literal: true

# This class handles the workspace
class Workspace
  attr_accessor :pathname

  IGNORE = %w[. .. .git .idea].freeze
  def initialize(pathname)
    @pathname = pathname
  end

  def list_files(path = @pathname)
    if File.directory?(path)
      filenames = Dir.entries(path) - IGNORE
      filenames.flat_map { |name| list_files(path.join(name)) }
    else
      [path.relative_path_from(@pathname)]
    end
  end

  def read_file(path)
    File.read(@pathname.join(path))
  end

  def stat_file(path)
    File.stat(@pathname.join(path))
  end
end
