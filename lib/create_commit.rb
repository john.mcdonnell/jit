# frozen_string_literal: true

require "workspace"
require "database"
require "database/blob"
require "database/commit"
require "entry"
require "database/tree"
require "database/author"
require "refs"

# This command creates a commit
class CreateCommit
  def initialize
    root_path = Pathname.new(Dir.getwd)
    git_path = root_path.join(".git")
    db_path = git_path.join("objects")
    workspace = Workspace.new(Pathname.new(Dir.getwd))
    database = Database.new(db_path)
    refs = Refs.new(git_path)

    entries = workspace.list_files.map do |path|
      data = workspace.read_file(path)
      blob = Database::Blob.new(data)
      database.store(blob)
      stat = workspace.stat_file(path)
      Entry.new(path, blob.oid, stat)
    end

    root = Database::Tree.build(entries)
    root.traverse { |tree| database.store(tree) }

    parent = refs.read_head
    name = ENV.fetch("GIT_AUTHOR_NAME", "John McDonnell")
    email = ENV.fetch("GIT_AUTHOR_EMAIL", "jmcdonnell@gitlab.com")
    author = Author.new(name, email, Time.now)
    message = $stdin.read
    commit = Database::Commit.new(parent, root.oid, author, message)
    database.store(commit)
    refs.update_head(commit.oid)

    is_root = parent.nil? ? "(root-commit) " : ""
    puts "[#{is_root}#{commit.oid}] #{message.lines.first}"
  end
end
