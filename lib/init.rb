# frozen_string_literal: true

# This command creates an empty Git repository
class Init
  def initialize
    path = ARGV.fetch(0, Dir.getwd)
    root_path = Pathname.new(File.expand_path(path))
    git_path = root_path.join(".git")
    %w[objects refs].each do |dir|
      FileUtils.mkdir_p(git_path.join(dir))
    rescue Errno::EACCES => e
      warn "fatal: #{e.message}"
      exit 1
    end
    puts "Initialized empty Jit repository in #{git_path}"
    exit 0
  end
end
