# frozen_string_literal: true

require "init"
require "create_commit"
require "add"

# This module is my implementation of git
module Jit
  class Error < StandardError
  end

  command = ARGV.shift
  case command

  when "init"
    Init.new
  when "commit"
    CreateCommit.new
  when "add"
    Add.new
  else
    puts "We don't support that yet!"
  end
end
